﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Arrow : MonoBehaviour {

    public Transform _selfTransform;
    private Vector3 _cashedPosition;
    public Rigidbody _rigid;
    public LayerMask _hitableLayer;
    public GameObject _go;
    public TrailRenderer _trail;
    public SoundType _hitTargetSound, _hitOtherSound;
	public GameObject _fire;
	private GameObject _lastFireEffect;
	public GameObject _missEfect;
	public Transform _playerPosition;

	void Start () {
        if (_selfTransform == null) {
            _selfTransform = transform;
        }
        if (_rigid == null) {
            _rigid = gameObject.GetComponent<Rigidbody>();
        }
        if (_go == null) {
            _go = gameObject;
        }
	}

	void Update () {
		Vector3 toFront = _selfTransform.position - _cashedPosition;
		if (Mathf.Abs (toFront.magnitude) > Mathf.Epsilon) {
			_selfTransform.rotation = Quaternion.LookRotation (toFront.normalized);            
		}
		_cashedPosition = _selfTransform.position;

		if (_selfTransform.position.y < 0f) { //場外に飛んでいったときの始末
			if (!(_lastFireEffect == null))
				Destroy (_lastFireEffect.gameObject);
			gameObject.SetActive (false);
			_rigid.isKinematic = true;
		}
	}

    void OnCollisionEnter ( Collision collision ) {

		Debug.Log ("shift operation : " + ((1 << collision.gameObject.layer)));
		Debug.Log ("_hitableLayer : " + _hitableLayer.value);
		Debug.Log ("bit operation : " + ((1 << collision.gameObject.layer) & _hitableLayer));

		if (!(_lastFireEffect == null))
			Destroy (_lastFireEffect.gameObject);
        if (((1 << collision.gameObject.layer) & _hitableLayer) != 0) {
            /*
             * 衝突したオブジェクトが _hitableLayerに含まれるとき。
             * LayerMaskは複数のレイヤーを指定できてInspector上では扱いやすいが、
             * データ上はビット演算になっていて、直感的でない。
             * 指定のレイヤーにだけ反応するようにするには、上記の書き方をすると効率的。
            */

            SoundSpawner.GetInstance().Play(_hitTargetSound, collision.contacts [0].point);

        } else {
			SoundSpawner.GetInstance().Play(_hitOtherSound, collision.contacts [0].point);
			Instantiate (_missEfect, this.transform.position, Quaternion.identity);
        }

        OnHit();
		PlayerMovement (this.transform);

    }

	void PlayerMovement(Transform posThisObject) {
		_playerPosition.position = posThisObject.position;
	}
    
    void OnHit () {
        gameObject.SetActive(false);
        _rigid.isKinematic = true;
        //Destroy(this.gameObject);
    }
		
    void OnSpawn () {
        _cashedPosition = _selfTransform.position;
    }

    public void Shoot (Vector3 pos,Quaternion rot,float force) {
        _selfTransform.position = pos;
        _selfTransform.rotation = rot;

        _trail.Clear();

        _go.SetActive(true);
        _rigid.isKinematic = false;
        _rigid.AddForce(_selfTransform.forward * force, ForceMode.VelocityChange);

		if (force >= 80) {
			
			GameObject _goChildTip = GameObject.FindWithTag ("tip");
			GameObject _fireEfect = Instantiate (_fire, _goChildTip.transform.position, Quaternion.identity) as GameObject;
			_fireEfect.transform.parent = _goChildTip.transform;
			_lastFireEffect = _fireEfect;

        


		}

    }
}
