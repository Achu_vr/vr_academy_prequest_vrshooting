﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class ArrowSpawner : MonoBehaviour {

    public GameObject _original;
    public List<Arrow> _instances;
    public int cashAmout = 5;

    private Transform _selfTranform;
    public Transform _player;


    private static ArrowSpawner _instance;

    private void Awake () {
        if (_instance == null) {
            _instance = this;
        } else if (_instance != null) {
            Destroy(this.gameObject);
        }
    }

    public static ArrowSpawner GetInstance () {
        return _instance;
    }

    private void OnDestroy () {
        if (_instance == this) {
            _instance = null;
        }
    }


    // Use this for initialization
    void Start () {
		if (_selfTranform == null) {
            _selfTranform = transform;
        }

        if (_player == null) {
            _player = Camera.main.transform;
        }

        if (_original.activeInHierarchy) {
            _original.SetActive(false);
        }

        for (int i = 0; i < cashAmout; i++) {
            SetCash();
        }

	}
	
    void SetCash () {
        GameObject ins = GameObject.Instantiate(_original, _selfTranform);
        _instances.Add(ins.GetComponent<Arrow>());
    }
    //
    void SetNewInstance () {
        GameObject ins = GameObject.Instantiate(_original,_player.position,_player.rotation, _selfTranform);
        Arrow arw = ins.GetComponent<Arrow>();
        _instances.Add(arw);        
    }

    //呼ばれたら生成しておいた弓オブジェクトを撃つ
    public void Shoot (float force) {
		if (_instances.Any (x => !x._go.activeInHierarchy)) {
			
			Arrow arw = _instances.FirstOrDefault (x => !x._go.activeInHierarchy);
			arw.Shoot (_player.position, _player.rotation, force);
		} else {
			SetNewInstance ();
		}
    }


}
